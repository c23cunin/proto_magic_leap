using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    private GameObject canva;

    private GameObject arrow; void Start() {
        canva = GameObject.Find("Canva");
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<LineRenderer>().SetPosition(0, new Vector3(canva.GetComponent<Transform>().position.x, canva.GetComponent<Transform>().position.y, canva.GetComponent<Transform>().position.z));       
    }
}
