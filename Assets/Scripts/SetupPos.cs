using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class SetupPos : MonoBehaviour
{
    private static Vector3 middlePos;

    private static Vector3 alertPos1;
    private static Vector3 alertPos2;
    private static Vector3 alertPos3;
    private static Vector3 alertPos4;


    public static void setMiddlePos(Vector3 pos) {
        middlePos = pos;
    }

    public static Vector3 getMiddlePos() {
        return middlePos;
    }


    //Alert 1
    public static void setAlertPos(Vector3 pos, int nbAlert) {
        switch(nbAlert) {
            case 1: 
                alertPos1 = pos;
                break;

            case 2:
                alertPos2 = pos;
                break;

            case 3:
                alertPos3 = pos;
                break;

            case 4:
                alertPos4 = pos;
                break;

            default:
                break;
        }
    }

    public static Vector3 getAlertPos(int nbAlert) {
        switch(nbAlert) {
            case 1: 
                return alertPos1;

            case 2:
                return alertPos2;

            case 3:
                return alertPos3;

            case 4:
                return alertPos4;

            default:
                return new Vector3(0, 0, 0);
        }
    }
}