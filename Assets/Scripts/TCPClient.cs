using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

using System.Collections;
using System.Collections.Generic;

using System.Threading.Tasks;
using System.Threading;

using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.Events;

using static OnAction;

public class TCPClient : MonoBehaviour
{
    // Every protocol typically has a standard port number. For example, HTTP is typically 80, FTP is 20 and 21, etc.
    // For this example, we'll choose an arbitrary port number.
    private string PortNumber = "1337";
    private Socket server;
    private byte[] data = new byte[1024];
    private int size = 1024;
    private int nbMsg = 0;
    private string msgCurrent = "";

    System.Net.Sockets.TcpClient client;
    System.Net.Sockets.NetworkStream stream;
    
    private bool exchanging = false;
    private Thread exchangeThread;
    private bool exchangeStopRequested = false;
    
    private StreamWriter writer;
    private StreamReader reader;

    //private GameObject camera;
    //private UnityEvent start = new UnityEvent();
    //private UnityEvent stop = new UnityEvent();
    
    void Start() {
        Debug.Log("launch");
        //camera = GameObject.Find("Main Camera");
        //start.AddListener(startAction);
        //stop.AddListener(stopAction);
        StartClient();
    }


    public async void StartClient() {
        /*IPHostEntry ipHostInfo = await Dns.GetHostEntryAsync("DESKTOP-JJ0H7CK");
        IPAddress ipAddress = ipHostInfo.AddressList[0];
        IPEndPoint ip = new(ipAddress, 8080);*/
        IPEndPoint ip = new IPEndPoint(IPAddress.Parse("10.29.225.78"), 8080);

        server = new Socket(AddressFamily.InterNetwork,SocketType.Stream, ProtocolType.Tcp);

        //using TcpClient server = new();

        try
        {
            await server.ConnectAsync(ip);

            Debug.Log("Connected with "+ IPAddress.Parse(((IPEndPoint)server.RemoteEndPoint).Address.ToString()) +" at port " + ((IPEndPoint)server.RemoteEndPoint).Port.ToString());

            /****** HERE
            client = new System.Net.Sockets.TcpClient("localhost", Int32.Parse(PortNumber));
            stream = client.GetStream();
            reader = new StreamReader(stream);
            writer = new StreamWriter(stream) { AutoFlush = true };

            exchangeStopRequested = false;
            exchangeThread = new System.Threading.Thread(ExchangePackets);
            exchangeThread.Start();
            ********/

            //using NetworkStream stream = server.GetStream();

            //while (msgCurrent != "exit") {
                /*var buffer = new byte[1_024];
                int received = stream.ReadAsync(buffer);

                var message = Encoding.UTF8.GetString(buffer, 0, received);
                Debug.Log("Message received : " + message);*/

                server.BeginReceive(data, 0, size, SocketFlags.None, new AsyncCallback(ReceiveData), server);
            
                /*if(msgCurrent == "exit") {
                    Debug.Log("coucou");
                    goto EndWhile;
                }*/
            //}

            //EndWhile;

        } 
        catch (SocketException e)
        {
            Debug.Log("Unable to connect to server." + e.ToString());
            return;
        }

       
        /*byte[] data = new byte[1024];
        int receivedDataLength = server.Receive(data);
        string stringData = Encoding.ASCII.GetString(data, 0, receivedDataLength);
        
        Debug.Log("receive msg : " + stringData);*/
    }

    void ReceiveData(IAsyncResult iar)
    {
        Socket remote = (Socket)iar.AsyncState;
        int recv = remote.EndReceive(iar);
        string stringData = Encoding.ASCII.GetString(data, 0, recv);

        //nbMsg;

        Debug.Log("receive msg : " + stringData);

        //msgCurrent = stringData;
        if(stringData == "exit") {
            closeConnexion();
        } else {
            server.BeginReceive(data, 0, size, SocketFlags.None, new AsyncCallback(ReceiveData), server);
        }

         if (String.Equals(stringData, "stopAlert")) {
            Debug.Log("isn't on action");
            OnAction.setIsOnAction(false);
            //stop.Invoke();
        }

        //hideView hide = new hideView();

        int found = stringData.IndexOf(",");

        if (stringData.Substring(0, found) == "startAlert") {
            Debug.Log("is on action");
            OnAction.setIsOnAction(true);
            OnAction.setIdAlert(int.Parse(stringData.Substring(found + 1)));
            //start.Invoke();
        }
    }

    /*public void ExchangePackets() {
        Debug.Log("Here");
        while (!exchangeStopRequested)
        {
            if (writer == null || reader == null) continue;
            exchanging = true;

            Debug.Log("Ready to read");

            writer.Write("X\n");
            Debug.Log("Sent data!");

            string received = null;

            byte[] bytes = new byte[client.SendBufferSize];
            int recv = 0;
            while (true)
            {
                recv = stream.Read(bytes, 0, client.SendBufferSize);
                received += Encoding.UTF8.GetString(bytes, 0, recv);

                if (received.EndsWith("\n")) break;
            }

            //lastPacket = received;
            Debug.Log("Read data: " + received);


            //hideView hide = new hideView();

            if (received == "exit\n") 
            {
                Debug.Log("Close");
                closeConnexion();
            }
                
            if (received == "startAlert\n") {
                Debug.Log("start hide");
                //hide.setHide(150);
                start.Invoke();
            }

            if (received == "stopAlert\n") {
                Debug.Log("stop hide");
                //hide.setHide(0);
                stop.Invoke();
            }

            Debug.Log("after if");

            exchanging = false;
        }
    }*/


    /*public void startAction () {
        camera.GetComponent<hideView>().setHide(150);
    }

    public void stopAction () {
        camera.GetComponent<hideView>().setHide(0);
    }*/


    public void closeConnexion() {
        server.Shutdown(SocketShutdown.Both);
        server.Close();
        Debug.Log("close connexion");

        /*exchangeStopRequested = true;
        
        if (exchangeThread != null)
        {
            exchangeThread.Abort();
            stream.Close();
            client.Close();
            writer.Close();
            reader.Close();

            stream = null;
            exchangeThread = null;
        }

        writer = null;
        reader = null;*/
    }
}
