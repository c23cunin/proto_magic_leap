using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

using static OnAction;

public class Action : MonoBehaviour
{
    private bool islaunch = false;
    private GameObject obj;
    private GameObject canva;

    public GameObject prefab_dot;
    public GameObject prefab_line;
    public GameObject prefab_map;
    public int RA_type; // 1 fleche    2 trait    3 point    4 map    5 cacher

    void Start() {
        canva = GameObject.Find("Canva");
        if(RA_type == 4) {
            Instantiate(prefab_map, canva.transform);
        }
    }

    void Update() {
        //Debug.Log("update action");
        if (OnAction.getIsOnAction() && !islaunch) 
        {
            Debug.Log("here");
            islaunch = true;

            switch(RA_type) {
                case 1:
                    break;

                case 2:
                    obj = Instantiate(prefab_line);
                    obj.GetComponent<LineRenderer>().positionCount = 2;
                    Vector3 pos4 = SetupPos.getAlertPos(OnAction.getIdAlert()); 
                    //Vector3 pos3 = new Vector3(pos4.x - 0.1f, pos4.y - 0.1f, pos4.z - 0.1f);
                    //Vector3 pos2 = new Vector3(pos4.x - 0.11f, pos4.y - 0.11f, pos4.z - 0.11f);
                    Vector3 pos1 = new Vector3(canva.GetComponent<Transform>().position.x, canva.GetComponent<Transform>().position.y, canva.GetComponent<Transform>().position.z);
                    obj.GetComponent<LineRenderer>().SetPositions(new Vector3 [2] {pos1, /*pos2, pos3, */pos4});
                    break;

                case 3:
                    Vector3 posInit = new Vector3(canva.GetComponent<Transform>().position.x, canva.GetComponent<Transform>().position.y, canva.GetComponent<Transform>().position.z);
                   
                    obj = Instantiate(prefab_dot, posInit, new Quaternion());
                    
                    break;

                case 4:
                    obj = GameObject.Find("Sphere" + OnAction.getIdAlert());
                    Debug.Log("Sphere" + OnAction.getIdAlert());
                    obj.GetComponentInChildren<Light>().intensity = 2;
                    obj.GetComponentInChildren<Renderer>().material.color = new Color32(241, 171, 171, 255);
                    break;

                case 5:
                    break;
                
                default:
                    Debug.Log("RA_type should be in [1 - 5]");
                    break;
            }
        } 
        else if (!OnAction.getIsOnAction() && islaunch) 
        {
            Debug.Log("Or here");
            islaunch = false;

            switch(RA_type) {
                case 1:
                    break;

                case 2:
                    Destroy(obj);
                    break;

                case 3:
                    Destroy(obj);
                    break;

                case 4:
                    obj.GetComponentInChildren<Light>().intensity = 0;
                    obj.GetComponentInChildren<Renderer>().material.color = new Color32(200, 0, 0, 139);
                    break;

                case 5:
                    break;
                
                default:
                    Debug.Log("RA_type should be in [1 - 5]");
                    break;
            }
        }
    }
}
