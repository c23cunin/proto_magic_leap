using System.Collections.Generic;
using System.Text;
using UnityEngine;

public static class OnAction
{
    private static bool isOnAction = false;
    private static int idAlert = 0;

    public static void setIsOnAction(bool action) {
        isOnAction = action;
        Debug.Log(isOnAction);
    }

    public static bool getIsOnAction() {
        //Debug.Log(isOnAction);
        return isOnAction;
    }

    public static void setIdAlert(int id) {
        idAlert = id;
    }

    public static int getIdAlert() {
        //Debug.Log(isOnAction);
        return idAlert;
    }
}