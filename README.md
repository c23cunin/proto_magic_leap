# proto_magic_leap


## Installation

```
git clone https://gitlab.imt-atlantique.fr/sos-dm/proto_magic_leap.git
```

Installer Unity Hub : https://unity.com/fr/download

Installer la version d'Unity 2022.2.20f1

Ouvrir le projet avec Unity Hub 

Suivre les étapes de la fenetre Magic Leap Setup Tool


Installer Magic Leap Hub : https://developer-docs.magicleap.cloud/docs/guides/getting-started/install-the-tools

Connecter le casque Magic Leap 2 dans device bridge (/!\ Pour la première connexion il faut connecter en filaire obligatoirement)


## Demarrage

Dans Unity App Sim Target > Target > Device > Play

Cliquer sur play pour lancer la simulation


## Configuration

### Pour changer le type de RA

Dans la fenetre Projet : Assets > Prefab > ClientManager

Dans la fenetre Inspector : rentrer RA_type (1 trait    2 fleche    3 point    4 map    5 cacher)

### Pour changer la taille des marqueurs Aruco

MarquerManager > Inspector > marker Tracker Example > Aruco Marker Size

